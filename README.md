# Color calc

A simple website to choose a color or convert an existing color to other formats.

See a preview here: [https://cami.codeberg.page/colorcalc/@main/](https://cami.codeberg.page/colorcalc/@main/)
