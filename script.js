/* event handlers */
document.getElementById("colorChoose").addEventListener("input", function (e) {
  getColor();
});

/* get color which is chosen by the input dialog */
function getColor() {
  var chosenColor = document
    .getElementById("colorChoose")
    .value.toString()
    .toUpperCase();
  document.getElementById("hexColor").innerText = chosenColor;
  hexToRgb(chosenColor);
}

/* if a hexcolor is only three chars long, 
his function will expand it so the hex-value 
as 6 characters */
function expandShortHex() {
  console.log("expandShortHex");
}

/* convert a rgb value to hex */
function rgbToHex() {
  console.log("rgbToHex");
}

/* Converts a six digit hex value to rgb */
function hexToRgb(hexColor) {
  hexClean = removeHash(hexColor);
  var red = parseInt(hexClean.substring(0, 2), 16);
  var green = parseInt(hexClean.substring(2, 4), 16);
  var blue = parseInt(hexClean.substring(4, 6), 16);
  generateRgbString(red, green, blue);
}

function generateRgbString(red, green, blue) {
  console.log(red);
  console.log(green);
  console.log(blue);
  document.getElementById("rgbColor").innerText =
    "" + red + " " + green + " " + blue;
}

/* Removes the hashtag in front of a hex value */
function removeHash(hexColor) {
  return hexColor.charAt(0) == "#" ? hexColor.substring(1, 7) : hexColor;
}
